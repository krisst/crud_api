<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
Use App\Comment;
use App\Post;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($post)
    {
        //
        $comments = Comment::where('post_id',$post)->get();
        return response()->json(['status' => 'success', 'data' => $comments, 'code'=>200],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $post)
    {
        //
        $validator = Validator::make($request->all(), [
          'messages' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors(), 'code'=>401], 401);            
        }
        $user = $request->user();

        $comment = new Comment;
        $comment->messages = $request->messages;
        $comment->user_id = $user->id;

        $post = Post::find($post);

        if($post->comments()->save($comment)){
            return response()->json(['status' => 'success', 'message' => 'Your Comment has been added to this post', 'code'=>201],201);
        }else{
            return response()->json(['status' => 'error', 'message' => 'Sorry Your comment cannot added in this post', 'code'=>400],400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($post, $id)
    {
        //
        $comments = Comment::where('post_id',$post)->where('id',$id)->get();
        return response()->json(['status' => 'success', 'data' => $comments, 'code'=>200],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $post, $id)
    {
        //
        $validator = Validator::make($request->all(), [
          'messages' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors(),'code'=>401], 401);            
        }

        $user = $request->user();

        $comment = Comment::find($id);
        $comment->messages = $request->messages;
        $comment->user_id = $user->id;
        $comment->post_id = $post;
        if($comment->save()){
            return response()->json(['status' => 'success', 'message' => 'Data has been updated', 'code'=>200],200);
        }else{
            return response()->json(['status' => 'error', 'message' => 'Data Has not been Updated', 'code'=>400],400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($post, $id)
    {
        //
        $comment = Comment::find($id);
        if ($comment) {
            $comment->delete();
            return response()->json(['stats' => 'success', 'message' => 'Data has been deleted', 'code'=>200], 200);
        }

        return response()->json(['status' => 'error', 'message' => 'Data not found', 'code'=>400],400);
    }
}
