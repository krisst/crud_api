<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    //
    public $successStatus = 200;

    public function login(Request $request){
        $validator = Validator::make($request->all(), [
			'email' => 'required|string|max:50',
			'password' => 'required|string',
			'client_id' => 'required|integer',
			'client_secret' => 'required|string'
		]);

		 if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }

		$email = preg_replace('/^[^a-zA-Z0-9@-_.]/', '', trim($request->input('email')));
		$password = $request->password;
		$client_id = $request->client_id;
		$client_secret = $request->client_secret;
	
				// $oauth_client = \DB::table('oauth_clients')->where('name','Laravel Password Grant Client')->first();
				$http = new \GuzzleHttp\Client;
				$response = $http->post(env('APP_DOMAIN','http://news.app').'/oauth/token', [
					'form_params' => [
					'grant_type' => 'password',
					'client_id' => $client_id,
					'client_secret' => $client_secret,
					'username' => $email, /// or any other network that your server is able to resolve.
					'password' => $password,
				],]);			
				// $data[''] = null; //create new method in illuminate/paginate/paginator.php   
				$result['status'] = true;
				$result['message'] = "Success anda bisa login";
				$result['data'] = json_decode($response->getBody(), true);
			
			$code = 200;
			return response()->json($result, $code);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        #$success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['name'] =  $user->name;

        return response()->json(['success'=>$success], $this->successStatus);
    }

    public function details(Request $request)
    {
        $user = $request->user;
        return response()->json(['success' => $user], $this->successStatus);
    }

    function refresh_token(Request $request){
    	$validator = Validator::make($request->all(), [
                'client_id' => 'required|integer',
                'client_secret' => 'required|string',
                'refresh_token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        $client_id = $request->client_id;
        $client_secret = $request->client_secret;
        $refresh_token = $request->refresh_token;

        $http = new \GuzzleHttp\Client;
            $response = $http->post('http://news.app/oauth/token', [
                'form_params' => [
                    'grant_type' => 'refresh_token',
                    'refresh_token' => $refresh_token,
                    'client_id' => $client_id,
                    'client_secret' => $client_secret,
                ],
            ]);

        return json_decode((string) $response->getBody(), true);
    }
}
