<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Post;
Use Validator;
use Illuminate\Database\QueryException as ErrorQuery;
use Illuminate\Database\Eloquent\RelationNotFoundException as ErrorRelation;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            // variable untuk query global
            $posts = Post::query();
            // variable untuk parameter searching
            $sorts = $request->get('sort');
            $array = explode(',', $sorts);
            // variable untuk parameter fields
            $fields = $request->get('field');
            $array1 = explode(',', $fields);
            // variable untuk parameter pencarian
            $title= $request->get('title');
            $body= $request->get('body');
            $created_at= $request->get('created_at');

            #percobaan filter
            $filter = $request->get('filter');
            $data = [];
            foreach ($posts->get() as $posts) {
                # code...
                $data[]['title'] = $posts->title;
                $data[]['body'] = $posts->body;
                $data[]['user_id'] = $posts->user_id;
            }
            dd($data);
            exit();
            
            /*if(isset($sorts)){
                foreach ($array as $sort) {
                    # code...
                    if(starts_with($sort, '-')){
                        $value = str_after($sort,'-');
                        $param = 'desc';  
                    }else{
                        $value = $sort;
                        $param = 'asc';
                    }
                    $posts->orderBy($value,$param);
                }
            }elseif(isset($fields)){
                foreach ($array1 as $field) {
                    # code...
                    $posts->addselect($field);
                }
            }else{
                $posts->orderBy('id');    
            }*/
            if(isset($fields)){
                if(isset($fields)){
                    foreach ($array1 as $field) {
                        # code...
                        $posts->addselect($field);
                    }    
                }
            }elseif(isset($sorts)){
                foreach ($array as $sort) {
                    # code...
                    if(starts_with($sort, '-')){
                        $value = str_after($sort,'-');
                        $param = 'desc';  
                    }else{
                        $value = $sort;
                        $param = 'asc';
                    }
                    $posts->orderBy($value,$param);
                }
            }elseif(isset($title)){
                $posts->where('title','LIKE','%'.$title.'%');
            }elseif(isset($body)){
                $posts->where('body','like','%'.$body.'%');
            }elseif(isset($created_at)){
                $posts->where('created_at','like','%'.$created_at.'%');
            }else{
                $posts->orderBy('id');    
            }

            $posts = $posts->orderBy('id')->get();

            $data = [];
            foreach ($posts as $key) {
                # code...
                $data[] ['title'] = $key->title;
                $data[] ['body'] = $key->body;
            }
            dd($data);
            exit();
            return response()->json(['status' => 'success', 'data' => $posts, 'code'=>200],200);
        } catch (ErrorQuery $e) {
            return response()->json(['status' => 'error', 'message' => 'Sorry, something went wrong', 'code'=>400],400);
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
          'title' => 'required',
          'body' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors(), 'code'=>401], 401);            
        }

        $user = $request->user();

        $post = new Post;
        $post->title = $request->title;
        $post->body = $request->body;
        $post->user_id = $user->id;
        $post->save();
 
        return response()->json(['status' => 'success', 'message' => 'Data has been created', 'code'=>201],201);
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
        try{
            #mengambil variable di url
            $embed = $request->get('embed');
            #cek apakah variable embed di ketik url
            if(isset($embed)){
                $posts = Post::with($embed)->find($id);
            }else{
                $posts = Post::find($id);
            }
            
            if ($posts) {
              return response()->json(['status' => 'success', 'data'=> $posts, 'code'=>200],200);
            }
     
            return response()->json(['status' => 'error', 'message' => 'Data Not Founc', 'code'=>200],200);

        }catch(QueryException $e){
            return response()->json(['status' => 'error', 'message' => 'Something went wrong', 'code'=>200],200);
        }catch(ErrorRelation $e){
            return response()->json(['status' => 'error', 'message' => 'Something went wrong', 'code'=>200],200);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), [
          'title' => 'required',
          'body' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors(), 'code'=>200], 401);            
        }

        $user = $request->user();

        $post = Post::find($id);
        $post->title = $request->title;
        $post->body = $request->body;
        $post->user_id = $user->id;
        if($post->save()){
            return response()->json(['status' => 'success', 'message' => 'Data has been created', 'code'=>200],201);
        }else{
            return response()->json(['status' => 'error', 'message' => 'Data Has not been Updated', 'code'=>400],400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $posts = Post::find($id);
        if ($posts) {
            $posts->delete();
            return response()->json(['stats' => 'success', 'message' => 'Data has been deleted', 'code'=>200]);
        }
        return response()->json(['status' => 'error', 'message' => 'Data not found', 'code'=>400],400);
    }
}
