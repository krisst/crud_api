<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'UserController@login');
Route::post('register', 'UserController@register');
Route::post('refresh_token', 'UserController@refresh_token');

Route::group(['middleware' => 'auth:api'], function () {
	#PostController
	Route::apiResource('post', 'PostController');
	Route::post('details', 'UserController@details');
	Route::post('/post/{post}/comments', 'CommentController@store');
	Route::put('/post/{post}/comments/{id}', 'CommentController@update');
	Route::delete('/post/{post}/comments/{id}', 'CommentController@destroy');
	
	#CommentController
	Route::get('/post/{post}/comments', 'CommentController@index');
	Route::get('/post/{post}/comments/{id}', 'CommentController@show');
});